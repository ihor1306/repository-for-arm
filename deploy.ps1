param(
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]
    $ResourceGroupName,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]
    $Location,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]
    $TemplateFile,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]
    $TemplateParameterFile
)

$password = ConvertTo-SecureString -String ${bamboo.PasswordSecret} -AsPlainText -Force
$credential = New-Object -TypeName "System.Management.Automation.PSCredential" -ArgumentList ${bamboo.UserSecret}, $password

Connect-AzAccount -Credential $credential

New-AzResourceGroup -Name $ResourceGroupName -Location $Location -Force

New-AzResourceGroupDeployment `
    -Mode "Incremental" `
    -Name "Deployment" `
    -ResourceGroupName $ResourceGroupName `
    -TemplateFile $TemplateFile `
    -TemplateParameterFile $TemplateParameterFile